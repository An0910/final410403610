﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameFunction : MonoBehaviour {

	public Text ScoreText;
	public int Score = 0;
	public static GameFunction Instance;


	// Use this for initialization
	void Start () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void AddScore(){
		Score += 10;
		ScoreText.text = "分數：" + Score;
	}
}
